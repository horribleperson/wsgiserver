Code of Conduct
===============

Follow common sense and basic courtesy rules. "Please" and "Thank you" will
take you much farther when asking questions or requesting help.

No harrassment of any kind will be tolerated, at all. No exceptions.


Compatibility
=============

All code changes should backwards compatible with Python 3.5 and (for the time
being) 2.7.


Workflow
========

This project uses the Gitflow workflow for contributions. New features or 
changes should be made in a branch based on `develop`. Code will be
merged from that branch to `develop` when it's ready, but never merged to
`master` directly. Once a feature or change is deemed complete, it will be 
then merged to `master` from `develop`. 

Release branches will be created from `master`. Final releases will be
tagged as such in the `master` branch, too.

Security fixes will be considered "hotfixes", and will be the only ones that
will be based on `master` instead of `develop`.

For more information, see Atlassian's excellent [Gitflow Workflow 
document](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)


Testing
=======

WSGIserver does not have a regression test suite, and that's unfortunate. Any
contributions of testing code will be gladly accepted.